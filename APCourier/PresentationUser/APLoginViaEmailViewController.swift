//
//  ASWLoginViaEmailViewController.swift
//  AutoSportWorld
//
//  Created by Aleksander Evtuhov on 15.12.2017.
//  Copyright © 2017 Кирилл Володин. All rights reserved.
//

import UIKit

class ASWLoginViaEmailViewController: ASWViewController, UITextFieldDelegate {
    
        override func viewDidLoad() {
            super.viewDidLoad()
            hideKeyboardWhenTappedAround()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
        setupTransparentNavBar()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setupUI(){
        ASWButtonManager.setupLoginButton(button: loginButton)
        
        
        loginField.blackBackgroundStyle = false
        loginField.placeHolder = "Логин"
        loginField.isPasswordField = false
        loginField.upperPlaceholderMode = false
        loginField.textField.addTarget(self, action: #selector(loginDidChange(_:)), for: .editingChanged)
        loginField.setupUI()
        
        passwordField.blackBackgroundStyle = false
        passwordField.isPasswordField = true
        passwordField.upperPlaceholderMode = false
        passwordField.placeHolder = "Пароль"
        passwordField.textField.addTarget(self, action: #selector(passwordDidChange(_:)), for: .editingChanged)
        passwordField.setupUI()

        
        loginField.textField.text = "test2"
        passwordField.textField.text = "test"
    }

    let emailValidator = ASWEmailValidator()
    let passwordValidator = ASWPasswordValidator()
    

    @IBOutlet weak var loginField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var passwordField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @objc func loginDidChange(_ sender: UITextField) {
        if var text = loginField.textField.text {
            text = emailValidator.format(text)
            sender.text = text
            loginField.incorrectMod = !(emailValidator.isValid(text) && !text.isEmpty)
        }
        updateFormValid()
    }
    
    
    @objc func passwordDidChange(_ sender: UITextField) {
        if var text = passwordField.textField.text {
            text = passwordValidator.format(text)
            sender.text = text
            passwordField.incorrectMod =  !(passwordValidator.isValid(text) && !text.isEmpty)
        }
        updateFormValid()
    }
    
    func updateFormValid(){
        loginButton.isEnabled = !passwordField.incorrectMod && !loginField.incorrectMod
    }
    
    func enterWaitMode(){
        ModalLoadingIndicator.show()
        loginButton.isEnabled = false
    }
    
    func leaveWaitMode(){
        DispatchQueue.main.async {
            [weak self] in
            ModalLoadingIndicator.hide()
            self?.updateFormValid()
        }
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        
        enterWaitMode()
        guard
            let login = loginField.textField.text,
            let password = passwordField.textField.text
            else { return }
        APNetworkManager.login(login: login,
                               password: password,
                               onSuccess: { [weak self] dto in
                                self?.leaveWaitMode()
                                guard let `self` = self else { return }
                                IS_LOGGED_IN = true
                                USER_ID = dto.id
                                USER = dto
                                self.performSegue(withIdentifier: APCourierRouter.Client.Segues.successClientLoginSegue, sender: nil) 
            },
                               onFailure: {
                                self.leaveWaitMode()
                                let alert = UIAlertController(title: "Внимание", message: "Неверная пара логин-пароль", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: { action in alert.dismiss(animated: true, completion: nil) }))
                                self.present(alert, animated: true, completion: nil)
        })
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
            let vc = ASWResetPasswordViewController.init(nibName: "ASWResetPasswordViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
}

