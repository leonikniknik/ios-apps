//
//  APUserProfileController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 29/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class APUserProfileController :UIViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    @IBOutlet weak var fioField: ASWLoginPasswordTextField!
    @IBOutlet weak var phoneField: ASWLoginPasswordTextField!
    
   
    @IBOutlet weak var photoImageView: UIImageView!
    
    @IBOutlet weak var addPhotoButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoHeight.constant = 44
        passportHeight.constant = 30
        //ASWButtonManager.setupVKButton(button: button)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForm()
        
        ASWButtonManager.setupVioletButton(button: addPhotoButton)
        addPhotoButton.setTitle("+", for: .normal)
        addPhotoButton.layer.cornerRadius = addPhotoButton.layer.frame.width/2
        addPhotoButton.clipsToBounds = true
    }
    
    func setupForm() {
        setupFIOField()
        setupPhoneField()
    }
    
    func setupFIOField() {
                    fioField.upperPlaceholderMode = true
                    fioField.blackBackgroundStyle = false
                    fioField.placeHolder = "ФИО"
                    fioField.upperPlaceHolder = "ФИО:"
                    fioField.textField.addTarget(self, action: #selector(fioDidChange(_:)), for: .editingChanged)
                    fioField.isPasswordField = false
                    fioField.textField.text = USER?.fio
                    fioField.setupUI()
    }
    
    func setupPhoneField() {
        phoneField.upperPlaceholderMode = true
        phoneField.blackBackgroundStyle = false
        phoneField.placeHolder = "Телефон"
        phoneField.upperPlaceHolder = "Телефон:"
        phoneField.textField.addTarget(self, action: #selector(phoneDidChange(_:)), for: .editingChanged)
        phoneField.isPasswordField = false
        phoneField.textField.text = USER?.phone
        phoneField.setupUI()
    }
    
    
    @objc func fioDidChange(_ sender: UITextField) {
                    if var text = fioField.textField.text {
                        fioField.incorrectMod =  text == ""
                    }
    }
    
    @objc func phoneDidChange(_ sender: UITextField) {
        if var text = fioField.textField.text {
            fioField.incorrectMod =  text == ""
        }
    }
    
    @IBOutlet weak var passportHeight: NSLayoutConstraint!
    
    @IBAction func resizePassport(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            if self.passportHeight.constant == 30 {
                self.passportHeight.constant = 150
            } else {
                self.passportHeight.constant = 30
            }
            self.view.layoutIfNeeded()
        })
    }
    
    @IBOutlet weak var photoHeight: NSLayoutConstraint!
    
    @IBAction func resizePhoto(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            if self.photoHeight.constant == 44 {
                self.photoHeight.constant = 200
            } else {
                self.photoHeight.constant = 44
            }
            self.view.layoutIfNeeded()
        })
    }
    
    
    
    var pickerController = UIImagePickerController()
    
    @IBAction func takePhoto(_ sender: Any) {
        let alertViewController = UIAlertController(title: "", message: "Choose your option", preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default, handler: { (alert) in
            self.openCamera()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default) { (alert) in
            self.openGallary()
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
            
        }
        
        alertViewController.addAction(camera)
        alertViewController.addAction(gallery)
        alertViewController.addAction(cancel)
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            pickerController.delegate = self
            self.pickerController.sourceType = UIImagePickerControllerSourceType.camera
            pickerController.allowsEditing = true
            self .present(self.pickerController, animated: true, completion: nil)
        }
        else {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = true
            self.present(pickerController, animated: true, completion: nil)
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var imageView = info[UIImagePickerControllerEditedImage] as! UIImage
        photoImageView.contentMode = .scaleAspectFill
        photoImageView.image = imageView
        dismiss(animated:true, completion: nil)
    }
    
    
}
