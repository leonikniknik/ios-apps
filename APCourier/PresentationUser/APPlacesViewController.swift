//
//  APPlacesViewController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 30/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class APPlacesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var places:[APPlaceDto] = [APPlaceDto]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PlaceCell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell") as! PlaceCell
        var place = places[indexPath.row]
        cell.nameLabel.text = place.title
        cell.addressLabel.text = place.address
        cell.big.isHidden = place.categories.filter({$0.id == 1}).count != 0
        cell.small.isHidden = place.categories.filter({$0.id == 2}).count != 0
        cell.eat.isHidden = place.categories.filter({$0.id == 3}).count != 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cnt = navigationController?.viewControllers.count ?? 0
        let vc = navigationController?.viewControllers[cnt - 2] as! APCreateOrderController
        var place = places[indexPath.row]
        vc.adressField.textField.text = place.address
        vc.adressField.setupUI()
        self.navigationController?.popViewController(animated: true)
        //self.dismiss(animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "PlaceCell", bundle: nil), forCellReuseIdentifier: "PlaceCell")
        getOrders()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }

    @IBOutlet weak var tableView: UITableView!
    
    func getOrders(){
        var place1 = """
 {
            "id": 1,
            "title": "Макдоналдс",
            "description": "Ресторан макдоналдс",
            "address": "Ладожская ул., 1/2",
            "phone": "88005553535",
            "categories": [{
                    "id": 1,
                    "name": "Большое",
                    "description": "Крупногабаритные товары"
                }]
        }
"""
        
        var place2 = """
        {
            "id": 21,
            "title": "Кафе Менора",
            "description": "Приятный интерьер, отличный сервис, приемлемые цены и отличные блюда русской, европейской и кавказской кухни, не оставят равнодушными гостей заведения.",
            "address": "ул. Первомайская, 19, Москва, Россия",
            "phone": "+7 495 603-85-88",
            "categories": [
                {
                    "id": 2,
                    "name": "Большое",
                    "description": "Крупногабаритные товары"
                },
                {
                    "id": 3,
                    "name": "Малое",
                    "description": "Мелкогабаритные товары"
                }
            ]
        }
"""
        var decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        var place1dto = try? decoder.decode(APPlaceDto.self, from: place1.data(using: .utf8)!)
        var place2dto = try? decoder.decode(APPlaceDto.self, from: place2.data(using: .utf8)!)
        places = [place1dto!,place2dto!]
    }
    
}
