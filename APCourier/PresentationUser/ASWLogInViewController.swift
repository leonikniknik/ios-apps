//
//  ASWLogInViewController.swift
//  AutoSportWorld
//
//  Created by Кирилл Володин on 26.08.17.
//  Copyright © 2017 Кирилл Володин. All rights reserved.
//

import UIKit

class ASWLogInViewController: ASWViewController {
 
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var vkButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descrTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupTransparentNavBar()
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    func setupUI(){
        imageView.backgroundColor = .white
        ASWButtonManager.setupLoginButton(button: loginButton)
        ASWButtonManager.setupLoginButton(button: registrationButton)
        ASWButtonManager.setupVKButton(button: vkButton)
        
        
//        titleLabel.textColor = UIColor.secondColorForText
//        descrTitle.textColor = UIColor.secondColorForText
        
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController) {
        present(controller, animated: true, completion: nil)
    }
   
}
