//
//  APCreateOrderController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 30/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit
import Segmentio

class APCreateOrderController :UIViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var nameField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var urlField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var date1Field: ASWLoginPasswordTextField!
    
    @IBOutlet weak var picker1Field: UIDatePicker!
    
    @IBOutlet weak var picker1Height: NSLayoutConstraint!
    
    
    @IBOutlet weak var priceField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var segmentio1: Segmentio!
    
    @IBOutlet weak var segmentio2: Segmentio!
    
    @IBOutlet weak var adressField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var createButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker1Height.constant = 0.0
        setupForm()
        setupSelectors()
        ASWButtonManager.setupLoginButton(button: createButton)
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createButton.layer.cornerRadius = createButton.frame.height/2
        createButton.clipsToBounds = true
    }
    
    func setupForm() {
        setupFIOField()
        setupURLField()
        setupDateField()
        setupPriceField()
        setupAdressField()
    }
    
    func setupFIOField() {
        nameField.upperPlaceholderMode = true
        nameField.blackBackgroundStyle = false
        nameField.placeHolder = "Название товара"
        nameField.upperPlaceHolder = "Название товара:"
        nameField.textField.addTarget(self, action: #selector(nameDidChange(_:)), for: .editingChanged)
        nameField.isPasswordField = false
        nameField.textField.text = ""
        nameField.setupUI()
    }
    
    @objc func nameDidChange(_ sender: UITextField) {
        if var text = nameField.textField.text {
            nameField.incorrectMod =  text == ""
        }
    }
    
    func setupURLField() {
        urlField.upperPlaceholderMode = true
        urlField.blackBackgroundStyle = false
        urlField.placeHolder = "Ссылка на товар"
        urlField.upperPlaceHolder = "Ссылка на товар:"
        urlField.textField.addTarget(self, action: #selector(urlDidChange(_:)), for: .editingChanged)
        urlField.isPasswordField = false
        urlField.textField.text = ""
        urlField.setupUI()
    }
    
    @objc func urlDidChange(_ sender: UITextField) {
        if var text = urlField.textField.text {
            urlField.incorrectMod =  text == ""
        }
    }
    
    func setupDateField() {
        date1Field.upperPlaceholderMode = true
        date1Field.blackBackgroundStyle = false
        date1Field.placeHolder = "Дата заказа"
        date1Field.upperPlaceHolder = "Дата заказа:"
        date1Field.isPasswordField = false
        date1Field.textField.text = ""
        date1Field.setupUI()
    }
    
    
    @IBAction func resizePicker1(_ sender: Any) {
        if self.picker1Height.constant == 0.0 {
            UIView.animate(withDuration: 1, animations: {
            self.picker1Height.constant = 200.0
            self.view.layoutIfNeeded()
            })
        } else {
            self.picker1Height.constant = 0.0
        }
    }
    
    
    @IBAction func dateChanged(_ sender: Any) {
        date1Field.textField.text = picker1Field.date.stringVal
        date1Field.setupUI()
    }
    
    func setupPriceField() {
        priceField.upperPlaceholderMode = true
        priceField.blackBackgroundStyle = false
        priceField.placeHolder = "Цена"
        priceField.upperPlaceHolder = "Цена:"
        priceField.textField.addTarget(self, action: #selector(priceDidChange(_:)), for: .editingChanged)
        priceField.isPasswordField = false
        priceField.textField.text = ""
        priceField.textField.keyboardType = .numberPad
        priceField.setupUI()
    }
    
    @objc func priceDidChange(_ sender: UITextField) {
        if var text = priceField.textField.text {
            priceField.incorrectMod =  text == ""
        }
    }
    
    func setupSelectors(){
        let m1 = SegmentioItem(
            title: "Покупка",
            image: UIImage()
        )
        let m2 = SegmentioItem(
            title: "Продажа",
            image: UIImage()
        )
        let content1 = [m1,m2]
        
        let m11 = SegmentioItem(
            title: "Еда",
            image: UIImage.init(named: "eat")
        )
        let m12 = SegmentioItem(
            title: "Мелкий пакет",
            image: UIImage.init(named: "small")
        )
        let m13 = SegmentioItem(
            title: "Крупные грузы",
            image: UIImage.init(named: "big")
        )
        let content2 = [m11,m12,m13]
        
        
        var options = SegmentioOptions(
            backgroundColor: .white,
            segmentPosition: SegmentioPosition.fixed(maxVisibleItems: 2),
            scrollEnabled: false,
            indicatorOptions: SegmentioIndicatorOptions(
                type: .bottom,
                ratio: 1,
                height: 2,
                color: UIColor.secondColorForText
            ),
            horizontalSeparatorOptions: SegmentioHorizontalSeparatorOptions(
                type: SegmentioHorizontalSeparatorType.topAndBottom, // Top, Bottom, TopAndBottom
                height: 1,
                color: .clear
            ),
            verticalSeparatorOptions: SegmentioVerticalSeparatorOptions(
                ratio: 0.6, // from 0.1 to 1
                color: .clear
            ),
            imageContentMode: .scaleAspectFit,
            labelTextAlignment: .center,
            segmentStates: SegmentioStates(
                defaultState: SegmentioState(
                    backgroundColor: .clear,
                    titleFont: UIFont.boldSystemFont(ofSize: 20.0),
                    titleTextColor: .black
                ),
                selectedState: SegmentioState(
                    backgroundColor: .clear,
                    titleFont: UIFont.boldSystemFont(ofSize: 20.0),
                    titleTextColor: .black
                ),
                highlightedState: SegmentioState(
                    backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                    titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
                    titleTextColor: .black
                )
            )
        )
        
        segmentio1.setup(
            content: content1,
            style: SegmentioStyle.imageOverLabel,
            options: options
        )
        
        segmentio1.selectedSegmentioIndex = 0
        
        options = SegmentioOptions(
            backgroundColor: .white,
            segmentPosition: SegmentioPosition.fixed(maxVisibleItems: 3),
            scrollEnabled: false,
            indicatorOptions: SegmentioIndicatorOptions(
                type: .bottom,
                ratio: 1,
                height: 2,
                color: UIColor.secondColorForText
            ),
            horizontalSeparatorOptions: SegmentioHorizontalSeparatorOptions(
                type: SegmentioHorizontalSeparatorType.topAndBottom, // Top, Bottom, TopAndBottom
                height: 1,
                color: .clear
            ),
            verticalSeparatorOptions: SegmentioVerticalSeparatorOptions(
                ratio: 0.6, // from 0.1 to 1
                color: .clear
            ),
            imageContentMode: .scaleAspectFit,
            labelTextAlignment: .center,
            segmentStates: SegmentioStates(
                defaultState: SegmentioState(
                    backgroundColor: .clear,
                    titleFont: UIFont.systemFont(ofSize: 15.0),
                    titleTextColor: .black
                ),
                selectedState: SegmentioState(
                    backgroundColor: .clear,
                    titleFont: UIFont.boldSystemFont(ofSize: 15.0),
                    titleTextColor: .black
                ),
                highlightedState: SegmentioState(
                    backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                    titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
                    titleTextColor: .black
                )
            )
        )
        
        segmentio2.setup(
            content: content2,
            style: SegmentioStyle.imageOverLabel,
            options: options
        )
        
        segmentio2.selectedSegmentioIndex = 0
        
    }
    
    func setupAdressField() {
        adressField.upperPlaceholderMode = true
        adressField.blackBackgroundStyle = false
        adressField.placeHolder = "Выберите адрес"
        adressField.upperPlaceHolder = "Адрес:"
        adressField.isPasswordField = false
        adressField.textField.text = ""
        adressField.setupUI()
    }
    
    
    @IBAction func addOrder(_ sender: Any) {
        self.showAlert(title: "Успех", message: "Заказ успешно создан", keyText: "ОК") {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}


