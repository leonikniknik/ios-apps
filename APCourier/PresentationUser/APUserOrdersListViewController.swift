//
//  APUserOrdersListViewController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 29/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit
import Segmentio


class APUserOrdersListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var ordersBuer: [APOrderDto] = [APOrderDto]()
    var ordersSeller: [APOrderDto] = [APOrderDto]()
    var orders:[APOrderDto] = [APOrderDto]()
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.secondColorForText
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.reloadOrders()
    }
    
    @IBOutlet weak var addBut: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentio: Segmentio!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Мои заказы"
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
       
        let buyer = SegmentioItem(
            title: "Заказы",
            image: UIImage()
        )
        let seller = SegmentioItem(
            title: "Продажи",
            image: UIImage()
        )
        let content = [seller,buyer]
        
        
        let options = SegmentioOptions(
            backgroundColor: .white,
            segmentPosition: SegmentioPosition.fixed(maxVisibleItems: 2),
            scrollEnabled: false,
            indicatorOptions: SegmentioIndicatorOptions(
                type: .bottom,
                ratio: 1,
                height: 2,
                color:  UIColor.secondColorForText
            ),
            horizontalSeparatorOptions: SegmentioHorizontalSeparatorOptions(
                type: SegmentioHorizontalSeparatorType.topAndBottom, // Top, Bottom, TopAndBottom
                height: 1,
                color: .lightGray
            ),
            verticalSeparatorOptions: SegmentioVerticalSeparatorOptions(
                ratio: 0.6, // from 0.1 to 1
                color: .clear
            ),
            imageContentMode: .scaleAspectFit,
            labelTextAlignment: .center,
            segmentStates: SegmentioStates(
                defaultState: SegmentioState(
                    backgroundColor: .clear,
                    titleFont: UIFont.systemFont(ofSize: 20.0),
                    titleTextColor: .black
                ),
                selectedState: SegmentioState(
                    backgroundColor: .clear,
                    titleFont: UIFont.boldSystemFont(ofSize: 20.0),
                    titleTextColor: .black
                ),
                highlightedState: SegmentioState(
                    backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                    titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
                    titleTextColor: .black
                )
            )
        )
        
        segmentio.setup(
            content: content,
            style: SegmentioStyle.imageOverLabel,
            options: options
        )
        
        segmentio.valueDidChange = { segmentio, segmentIndex in
            print("Selected item: ", segmentIndex)
            self.tableviewReloadData()
        }
        
        segmentio.selectedSegmentioIndex = 0
        tableView.register(UINib(nibName: "APOrderCell", bundle: nil), forCellReuseIdentifier: "APOrderCell")
        tableView.register(UINib(nibName: "APEmptyCellTableViewCell", bundle: nil), forCellReuseIdentifier: "APEmptyCellTableViewCell")
        
        
        
    
        
        tableviewReloadData()
        reloadOrders()
        self.tableView.refreshControl = refreshControl
        self.tableView.refreshControl?.beginRefreshing()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let gradient = CAGradientLayer()
//        gradient.frame = downGradView.bounds
        gradient.colors = [UIColor.white.withAlphaComponent(0.0).cgColor, UIColor.red.cgColor]
        //downGradView.layer.addSublayer(gradient)
//        downGradView.addGrad(topColor: UIColor.white.withAlphaComponent(0), bottomColor: .white)
        //ASWButtonManager.setupVioletButton(button: addBut)
        //addBut.backgroundColor = UIColor.APColor.appBlue
        //addBut.addGrad(topColor: .red, bottomColor: .blue)
        
        //addBut.addRoundCorner(cornerRadius: addBut.frame.width/2)
        
    }
    
    func tableviewReloadData(){
        if segmentio.selectedSegmentioIndex == 0 {
            orders = ordersBuer
        } else {
            orders = ordersSeller
        }
        
        self.tableView.reloadData()
    }
    
    func reloadOrders(){
        APNetworkManager.getAllOrders(role:2,status:"0,1,2,3,4,5",success: { [weak self] array in
            guard let `self` = self else { return }
            let myID = USER?.id ?? 0
            self.ordersBuer = array.filter {
               return $0.buyer.id == myID
            }
            
            self.ordersSeller = array.filter {
                return $0.seller.id == myID
            }
            
            self.tableviewReloadData()
            self.tableView.refreshControl?.endRefreshing()
            
            }, failure: { [weak self] in
                self?.tableviewReloadData()
                self?.tableView.refreshControl?.endRefreshing()
                self?.showAlert(title: "Ошибка", message: "Ошибка загрузки заказов", keyText: "Ок")
                
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orders.count == 0 {
            return 1
        }
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "APOrderCell") as! APOrderCell
        if orders.count == 0 {
            let emptyCell =  tableView.dequeueReusableCell(withIdentifier: "APEmptyCellTableViewCell") as! APEmptyCellTableViewCell
            return emptyCell
        }
        let order:APOrderDto = orders[indexPath.row]
        
        
        cell.roundedCell.layer.cornerRadius = 20
        cell.roundedCell.clipsToBounds = true
        
        
        cell.catImage.image = order.category.getCatPic()
        cell.titleLabel.text = order.product
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        
        cell.dateLabel.text = (order.acceptanceDate ?? Date()).stringVal
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if orders.count == 0 {
            return
        }
        let order:APOrderDto = orders[indexPath.row]
        self.performSegue(withIdentifier: APCourierRouter.Courier.Segues.orderDetailSegue, sender: order)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == APCourierRouter.Courier.Segues.orderDetailSegue{
            let destinationViewController = segue.destination as! APOrderScreenViewController
            destinationViewController.order = sender as! APOrderDto
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if orders.count == 0 {
            return 600
        } else {
            return 110
        }
    }
    
    @IBAction func addNew(_ sender: Any) {
    }
    
    
    
}



