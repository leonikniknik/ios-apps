//
//  ASWRegisterAccountViewController.swift
//  AutoSportWorld
//
//  Created by Aleksander Evtuhov on 04.01.2018.
//  Copyright © 2018 Кирилл Володин. All rights reserved.
//

//
//  ASWRegistrationViewController.swift
//  AutoSportWorld
//
//  Created by Кирилл Володин on 18.08.17.
//  Copyright © 2017 Кирилл Володин. All rights reserved.
//

import UIKit

class ASWBasePasswordViewController: UIViewController, UITextFieldDelegate {

    let passwordValidator = ASWPasswordValidator()
    var password = ""
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var passwordField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var repeatPasswordField: ASWLoginPasswordTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
//        setupTransparentNavBar()
        ASWButtonManager.setupLoginButton(button: button)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupForm()
    }
    override func viewDidLayoutSubviews() {
        
    }
    
    func setupForm() {
        setupPasswordField()
        setupRepeatPasswordField()
    }
    
    func setupPasswordField() {
        passwordField.upperPlaceholderMode = true
        passwordField.blackBackgroundStyle = false
        passwordField.placeHolder = "Придумайте пароль:"
        passwordField.upperPlaceHolder = "Придумайте пароль (6 знаков):"
        passwordField.textField.addTarget(self, action: #selector(passwordDidChange(_:)), for: .editingChanged)
        passwordField.isPasswordField = true
        passwordField.setupUI()
    }
    
    func setupRepeatPasswordField() {
        repeatPasswordField.upperPlaceholderMode = true
        repeatPasswordField.blackBackgroundStyle = false
        repeatPasswordField.placeHolder = "Повторите пароль:"
        repeatPasswordField.upperPlaceHolder = "Повторите пароль:"
        repeatPasswordField.textField.addTarget(self, action: #selector(repeatPasswordDidChange(_:)), for: .editingChanged)
        repeatPasswordField.isPasswordField = true
        repeatPasswordField.setupUI()
    }

    @objc func passwordDidChange(_ sender: UITextField) {
        if var text = passwordField.textField.text {
            text = passwordValidator.format(text)
            sender.text = text
            password = text
            passwordField.incorrectMod =  !(passwordValidator.isValid(text) && !text.isEmpty)
        }
    }
    
    
    @objc func repeatPasswordDidChange(_ sender: UITextField) {
        if var text = repeatPasswordField.textField.text {
            
            text = passwordValidator.format(text)
            sender.text = text
            
            repeatPasswordField.incorrectMod = !(passwordValidator.isValid(text) &&  text == passwordField.textField.text)
        }
    }
    
    func isFormValid() -> Bool {
        guard passwordValidator.isValid(passwordField.textField.text ?? "") else {
            return false
        }
        
        guard passwordValidator.isValid(repeatPasswordField.textField.text ?? "") else {
            return false
        }
        
        guard passwordField.textField.text ?? "-1" == repeatPasswordField.textField.text ?? "1" else {
            return false
        }
        
        return true
    }
}

class ASWRegisterAccountViewController:ASWBasePasswordViewController {
    
    let emailValidator = ASWEmailValidator()
    let nameValidator = ASWNameValidator()
    
    var email = ""
    var name = ""
    
    @IBOutlet weak var emailField: ASWLoginPasswordTextField!
    
    @IBOutlet weak var nameField: ASWLoginPasswordTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupBlackOpaqueNavBar()
        setupForm()
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    override func setupForm() {
        super.setupForm()
        setupNameField()
        setupEmailField()
    }
    
    func setupNameField() {
        nameField.upperPlaceholderMode = true
        nameField.blackBackgroundStyle = false
        nameField.isPasswordField = false
        nameField.placeHolder = "Введите имя:"
        nameField.upperPlaceHolder = "Введите имя:"
        nameField.textField.addTarget(self, action: #selector(nameDidChange(_:)), for: .editingChanged)
        nameField.setupUI()
    }
    
    func setupEmailField() {
        emailField.upperPlaceholderMode = true
        emailField.blackBackgroundStyle = false
        emailField.isPasswordField = false
        emailField.placeHolder = "Введите адрес эл. почты:"
        emailField.upperPlaceHolder = "Введите адрес эл. почты:"
        emailField.textField.addTarget(self, action: #selector(emailDidChange(_:)), for: .editingChanged)
        emailField.setupUI()
    }
    
    @objc func emailDidChange(_ sender: UITextField) {
        if var text = emailField.textField.text {
            text = emailValidator.format(text)
            sender.text = text
            email = text
            emailField.incorrectMod = !(emailValidator.isValid(text) && !text.isEmpty)
        }
    }
    
    
    @objc override func passwordDidChange(_ sender: UITextField) {
        super.passwordDidChange(sender)
        //delegate?.updateUserLoginInfo(valid: isFormValid(), login: name, email: email, password: password)
    }
    
    @objc override func repeatPasswordDidChange(_ sender: UITextField) {
        super.repeatPasswordDidChange(sender)
        //delegate?.updateUserLoginInfo(valid: isFormValid(), login: name, email: email, password: password)
    }
    
    @objc func nameDidChange(_ sender: UITextField) {
        if var text = nameField.textField.text {
            name = text
            text = nameValidator.format(text)
            sender.text = text
            nameField.incorrectMod = !nameValidator.isValid(text)
        }
        //delegate?.updateUserLoginInfo(valid: isFormValid(), login: name, email: email, password: password)
    }
    
    func fillFormFromUserModel(){
        nameField.textField.text = name
        emailField.textField.text = email
        repeatPasswordField.textField.text = password
        passwordField.textField.text = password
        setupForm()
    }
    
    override func isFormValid() -> Bool {
        if !super.isFormValid(){
           return false
        }
        
        if let nameData = nameField.textField.text {
            guard nameData.count != 0 else {
                return false
            }
        }
        else {
            return false
        }
        
        guard emailValidator.isValid(emailField.textField.text ?? "") else {
            return false
        }
        
        return true
    }
    
    
    @IBAction func register(_ sender: Any) {
        guard
            let login = emailField.textField.text,
            let password = passwordField.textField.text,
            let rePassword = repeatPasswordField.textField.text
            else { return }
        
        if password != rePassword {
            showAlert(title: "Ошибка", message: "Пароли не совпадают", keyText: "Ок")
            return
        }
        
        APNetworkManager.register(login: login,
                                  password: password,
                                  success: { [weak self] in
                                    guard let `self` = self else { return }
                                    IS_LOGGED_IN = false
                                    USER_ID = -1
                                    self.showAlert(title: "ОК", message: "ОК", keyText: "Ок") {
                                    self.navigationController?.popViewController(animated: true)
                                    }
                                    
            },
                                  failure: { [weak self] in
                                    guard let `self` = self else { return }
                                    self.showAlert(title: "Ошибка", message: "Уже существует", keyText: "Ок")
        })
    }
    
    
}
