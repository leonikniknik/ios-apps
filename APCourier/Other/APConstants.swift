//
//  APConstants.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation

let defaults = UserDefaults.standard
var ORDERS: [APOrderDto]?

public var USER_ID: Int {
    get {
        return defaults.integer(forKey: "USER_ID")
    }
    
    set {
        defaults.set(newValue, forKey: "USER_ID")
    }
}

public var USER: APLoginDto? {
    get {
        let decoder = JSONDecoder()
        let data = defaults.data(forKey: "USER") ?? Data()
        return try? decoder.decode(APLoginDto.self, from: data)
    }
    
    set {
        let encoder = JSONEncoder()
        guard let user = newValue else { return }
        defaults.set(try? encoder.encode(user), forKey: "USER")
    }
}

public var IS_LOGGED_IN: Bool {
    get {
        return defaults.bool(forKey: "IS_LOGGED_IN")
    }
    
    set {
        defaults.set(newValue, forKey: "IS_LOGGED_IN")
    }
}

public var SEND_GEO_FLAG: Bool {
    get {
        return defaults.bool(forKey: "SEND_GEO_FLAG")
    }
    
    set {
        defaults.set(newValue, forKey: "SEND_GEO_FLAG")
    }
}

public var SEND_GEO_FREQ: Int {
    get {
        return defaults.integer(forKey: "SEND_GEO_FREQ")
    }
    
    set {
        defaults.set(newValue, forKey: "SEND_GEO_FREQ")
    }
}
