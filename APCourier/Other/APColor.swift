//
//  APColor.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 29/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit
extension UIColor {
    
    struct APColor {
        
        static let red = UIColor.init(red: 237/255.0, green: 51/255.0, blue: 35/255.0, alpha: 1.0)
        
        static let black = UIColor.init(red: 35/255.0, green: 35/255.0, blue: 35/255.0, alpha: 1.0)
        
        static let yellow = UIColor.init(red: 223/255.0, green: 165/255.0, blue: 69/255.0, alpha: 1.0)
        
        static let grey = UIColor.init(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)
        
        static let lightGrey = UIColor.init(red: 141/255.0, green: 141/255.0, blue: 141/255.0, alpha: 1.0)
        
        static let greyBackground = UIColor.init(red: 243/255.0, green: 243/255.0, blue: 243/255.0, alpha: 1.0)
        
        static let pink = UIColor.init(red: 251/255.0, green: 24/255.0, blue: 104/255.0, alpha: 1.0)
        
        static let darkBlue = UIColor(red: 0/255.0, green: 72/255.0, blue: 160/225.0, alpha: 1)
        
        static let yellowSelection = UIColor(red: 224/255.0, green: 148/255.0, blue: 31/225.0, alpha: 1)
        
        static let appBlue = UIColor(red: 37/255, green: 98/255, blue: 147/255, alpha: 1)

        static let appViolet = UIColor(red: 109/255, green: 140/255, blue: 251/255, alpha: 1)
        
    }

    static let commonGrey = UIColor.init(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
    
    static let firstColorForText = UIColor.init(red: 144/255, green: 143/255, blue: 157/255, alpha: 1)

    static let secondColorForText = UIColor.init(red: 45/255, green: 45/255, blue: 65/255, alpha: 255/255)
    
    static let lightRed = UIColor.init(red: 255/255, green: 92/255, blue: 92/255, alpha: 1)
    
    
}
