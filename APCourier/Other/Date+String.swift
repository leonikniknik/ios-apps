//
//  Date+String.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation
extension Date {
    var stringVal:String{
        get {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
            return dateFormatter.string(from: self)
        }
    }
}
