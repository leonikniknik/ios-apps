//
//  AppDelegate.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit
import Alamofire
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        self.window = UIWindow(frame: UIScreen.main.bounds)
        IQKeyboardManager.shared.enable = true
        
        
        if true {
            var mainStoryboard: UIStoryboard = UIStoryboard(name: "Registration", bundle: nil)
            var viewController: UIViewController = mainStoryboard.instantiateInitialViewController()!
            if IS_LOGGED_IN {
               mainStoryboard = UIStoryboard(name: "Registration", bundle: nil)
                viewController = mainStoryboard.instantiateViewController(withIdentifier: "ClientOrdersViewController")
            }
            self.window?.rootViewController = viewController
        } else {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            var viewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController")
            if IS_LOGGED_IN {
                viewController = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController")
            }
            self.window?.rootViewController = viewController
        }
        
        self.window?.makeKeyAndVisible()
        return true
    }
}

