//
//  APNetworkService.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Alamofire
import CodableAlamofire

extension APNetworkManager {
    static func request<T:Decodable>(URL: String, method: HTTPMethod, parameters: Parameters, encoding: ParameterEncoding, onSuccess: @escaping (T) -> Void, onError: @escaping (Int) -> Void) -> Void {
        print("requesting URL \(URL)")
       
        let headers = ["Content-Type":"application/json","X-Auth-Token":"\(USER?.id ?? -1)"]
        //let headers = ["Content-Type":"multipart/form-data","X-Auth-Token":"\(USER_ID)"]
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        //decoder.dateDecodingStrategy = .secondsSince1970
        let manager = Alamofire.SessionManager.default
        //manager.session.configuration.timeoutIntervalForRequest = 5
        manager.request(URL, method: method, parameters: parameters,encoding: encoding, headers: headers ).validate().responseDecodableObject(decoder: decoder) { (response: DataResponse<T>) in
            
            switch response.result {
            case .success(let value):
                //print(value)
                onSuccess(value)
            case .failure(let error):
                if let afError:AFError = error as? AFError {
                    let code = afError.responseCode ?? 0
                    print(code)
                    onError(code)
                } else {
                    print(error)
                    onError(-1)
                }
               
            }
        }
    }

}
