//
//  APCNetworkManager.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit
import CodableAlamofire
import Alamofire

class APNetworkManager {
    static func login(login:String, password:String, onSuccess:@escaping (APLoginDto)->Void, onFailure:@escaping ()->()) {
        
        func success(dto:APLoginContainerDto) -> Void{
            guard let res = dto.result else {
                onFailure()
                return
            }
            onSuccess(res)
        }
        
        func onerror(error: Any?) -> Void {
            onFailure()
        }
        
        let request = APLoginRequest(login: login, password: password)
        APNetworkManager.request(URL: request.url, method: .post, parameters: request.parameters,encoding: request.encoding, onSuccess: success, onError: onerror)
    }
    
    static func register(login:String, password:String, success:@escaping ()->Void, failure:@escaping ()->()) {
        
        func onSuccess(dto:APEmptyDto) -> Void{
            success()
        }
        
        func onError(error: Any?) -> Void {
            failure()
        }
        
        let request = APRegisterRequest(login: login, password: password)
        
        APNetworkManager.request(URL: request.url, method: .post, parameters: request.parameters,encoding: request.encoding, onSuccess: onSuccess, onError: onError)
    }
    
    static func getAllOrders(role:Int,status:String,success:@escaping ([APOrderDto])->Void, failure:@escaping ()->()) {
        
        func onSuccess(dto:APOrdersContainerDto) -> Void{
            success(dto.result)
        }
        
        func onError(error: Any?) -> Void {
            failure()
        }
        
        let request = APAllOrdersRequest(role: role, status: status)
        
        APNetworkManager.request(URL: request.url, method: .get, parameters: request.parameters,encoding: request.encoding, onSuccess: onSuccess, onError: onError)
    }
    
    static func getOrders(success:@escaping ([APOrderDto])->Void, failure:@escaping ()->()) {
        
        func onSuccess(dto:APOrdersContainerDto) -> Void{
            success(dto.result)
        }
        
        func onError(error: Any?) -> Void {
            failure()
        }
        
        let request = APOrdersRequest()
        
        APNetworkManager.request(URL: request.url, method: .get, parameters: request.parameters,encoding: request.encoding, onSuccess: onSuccess, onError: onError)
    }
    
    
//    static func getSomething(request: APRequest) {
//
//        func onSuccess(json: Example) -> Void{
//
//        }
//
//        func onError(error: Any) -> Void {
//
//        }
//
//        ASWNetworkManager.request(URL: request.url, method: .get, parameters: request.parameters,encoding: request.encoding, onSuccess: onSuccess, onError: onError)
//    }
//
//    static func postPic(request: APRequest) {
//
//        func onSuccess(json: Example) -> Void{
//
//        }
//
//        func onError(error: Any) -> Void {
//
//        }
//
//        ASWNetworkManager.requestPicture(URL: request.url, method: .get, parameters: request.parameters,encoding: request.encoding, onSuccess: onSuccess, onError: onError)
//    }
    
    
    
    

}
