//
//  APAuthDto.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

public struct APLoginContainerDto: Codable  {
    var result: APLoginDto?
}

public struct APLoginDto: Codable {
    var id: Int
    var login: String
    var fio: String?
    var phone: String
    var passport: String
    var passportPhoto: String
    var naviAddress: APNaviDto?
    var sendFrequency: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case login
        case phone
        case fio
        case passport
        case passportPhoto = "passport_photo"
        case naviAddress = "navi_address_id"
        case sendFrequency = "send_frequency"
    }
}
