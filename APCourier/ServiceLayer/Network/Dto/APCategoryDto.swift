//
//  APCategoryDto.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

public enum APCategoty: Int {
    case eat = 0
    case big = 1
    case small = 2
}

public struct APCategoryDto: Codable {
    var id: Int
    var name: String
    var description: String
    
    func getCat()->APCategoty{
        return APCategoty(rawValue: id) ?? .small
    }
    
    func getCatPic() -> UIImage{
        
        switch getCat() {
        case .eat:
            return #imageLiteral(resourceName: "eat")
        case .big:
            return #imageLiteral(resourceName: "big")
        case .small:
            return #imageLiteral(resourceName: "small")
        }
    }
}

