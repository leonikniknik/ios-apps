//
//  APOrdersDto.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation

public struct APOrdersContainerDto: Codable  {
    var result: [APOrderDto]
}

public struct APOrderDto: Codable  {
        var id: Int
        var seller: APLoginDto
        var buyer: APLoginDto
        var place: APPlaceDto
        var category: APCategoryDto
        var product: String
        var productUrl: String
        var status: Int
        var price: Int
    
    var acceptanceDate: Date
    var deliveryDate: Date
        
        enum CodingKeys: String, CodingKey {
            case id
            case seller = "seller"
            case buyer = "buyer"
            case place = "place"
            case category = "category"
            case product
            case productUrl = "product_url"
            case acceptanceDate = "acceptance_date"
            case deliveryDate = "delivery_date"
            case price
            case status
        }

}
