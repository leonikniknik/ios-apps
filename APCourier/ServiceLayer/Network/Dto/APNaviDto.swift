//
//  APNaviDto.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 28/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation

public struct APNaviDto: Codable {
    var id: Int
    var container: String
    var naviaddress: String
    var name: String
}
