//
//  APPlaceDto.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation

public struct APPlaceDto: Codable {
    var id: Int
    var title: String
    var description: String
    var naviAddress: APNaviDto?
    var address: String
    var phone: String
    var categories: [APCategoryDto]

    enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case naviAddress = "navi_address"
        case address
        case phone
        case categories
    }
}

