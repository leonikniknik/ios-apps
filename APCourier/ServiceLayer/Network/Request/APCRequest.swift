//
//  APCRequest.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Alamofire

class APRequest {
    var baseURL = "http://rovragge.ddns.net:8000/api"
    var parameters: Parameters = [:]
    var url: String = ""
    var encoding: ParameterEncoding = URLEncoding.default
}
