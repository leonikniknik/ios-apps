//
//  APGeoRequest.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation
import Alamofire

class APGeoRequest: APRequest {
    init(latitude: Double, longitude: Double) {
        super.init()
        
        url = self.baseURL + "/user/register"
        parameters["latitude"] = latitude
        parameters["longitude"] = longitude
        encoding = JSONEncoding.default
    }
}
