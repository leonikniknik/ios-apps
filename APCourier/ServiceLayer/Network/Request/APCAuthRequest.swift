//
//  APCAuthRequest.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Alamofire



class APLoginRequest: APRequest {
    init(login: String, password: String) {
        super.init()
        
        url = self.baseURL + "/user/login"
        parameters["login"] = login
        parameters["password"] = password
        encoding = JSONEncoding.default
    }
}

class APRegisterRequest: APRequest {
    init(login: String, password: String) {
        super.init()
        
        url = self.baseURL + "/user/register"
        parameters["login"] = login
        parameters["password"] = password
        encoding = JSONEncoding.default
    }
}
