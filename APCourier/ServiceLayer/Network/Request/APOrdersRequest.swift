//
//  APOrdersRequest.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation
import Alamofire

class APOrdersRequest: APRequest {
    override init() {
        super.init()
        url = self.baseURL + "/user/orders"
        encoding = JSONEncoding.default
    }
}

class APAllOrdersRequest: APRequest {
    init(role:Int,status:String) {
        super.init()
        url = self.baseURL + "/user/orders?role=\(role)&statuses=\(status)"
        encoding = JSONEncoding.default
    }
}
