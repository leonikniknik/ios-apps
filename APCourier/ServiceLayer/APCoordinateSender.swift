//
//  APCoordinateSender.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation
import CoreLocation

class APCoordinateSender{
    private static var senderQueue: DispatchQueue?
    private static var workItem: DispatchWorkItem?
    static var send: Bool = false
    
    
    static func startSending(){
        self.send = true
        if let queue = senderQueue {
            stopSending()
        } else {
            senderQueue =  DispatchQueue(label: "coordinateSender", qos: .utility, attributes: [])
            var w: DispatchWorkItem!
            w = DispatchWorkItem(block: {
                while self.send {
                    print("send")
                    sleep(1)
                }
            })
            workItem = w
            senderQueue?.async(execute: workItem!)
        }
    }
    
    static func stopSending(){
        self.send = false
    }
}
