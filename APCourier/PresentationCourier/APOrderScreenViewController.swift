//
//  APOrderScreenViewController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class APOrderScreenViewController: UIViewController {
    
    var order: APOrderDto!
    
    @IBOutlet weak var reciverCode: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var webView: UIWebView!
    
    
    @IBOutlet weak var date2Label: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var bt1: UIButton!
    
    @IBOutlet weak var bt2: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = order.product
        reciverCode.text = "\(order.buyer.id)"
        addressLabel.text = order.place.address
        categoryLabel.text = order.category.description
        dateLabel.text = (order.deliveryDate ?? Date()).stringVal
        date2Label.text = (order.acceptanceDate ?? Date()).stringVal
        var status = ""
        switch order.status {
        case 0:
            status = "Ожидает подтверждения"
            statusLabel.textColor = .red
        case 1:
            status = "Идет доставка на точку"
            statusLabel.textColor = .yellow
        case 2:
            status = "Ожидает на точке"
            statusLabel.textColor = .green
        case 3:
            status = "Завершен"
            statusLabel.textColor = UIColor.APColor.appViolet
        default:
            ""
        }
        
        statusLabel.text = status
        if let url=URL(string: order.productUrl) {
           webView.loadRequest(URLRequest(url: url))
        }
        
        ASWButtonManager.setupLoginButton(button: bt1)
        
        ASWButtonManager.setupLoginButton(button: bt2)
        
    }
    
    @IBOutlet weak var openNavi: UIButton!
    
    @IBAction func goTo(_ sender: Any) {
        performSegue(withIdentifier: APCourierRouter.Courier.Segues.naviAdressSegue, sender: order.place.naviAddress)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == APCourierRouter.Courier.Segues.naviAdressSegue {
            let destinationViewController = segue.destination as! APNaviAdressViewController
            destinationViewController.strUrl = "https://staging.naviaddress.com/" + (order?.place.naviAddress?.container ?? "") + "/" + (order.place.naviAddress?.naviaddress ?? "")
            print(destinationViewController.strUrl)
            //destinationViewController.strUrl = "https://staging.naviaddress.com/7/703945"
        }
    }
}

