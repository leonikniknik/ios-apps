//
//  APOrdersList.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit


class APOrdersListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var orders: [APOrderDto] = [APOrderDto]()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.reloadOrders()
    }
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Мои заказы"
        self.tableView.dataSource = self
        self.tableView.delegate = self

        
        tableView.register(UINib(nibName: "APOrderCell", bundle: nil), forCellReuseIdentifier: "APOrderCell")
        
        
        self.tableView.reloadData()
        reloadOrders()
        self.tableView.refreshControl = refreshControl
        self.tableView.refreshControl?.beginRefreshing()
        
    }
    
    func reloadOrders(){
        APNetworkManager.getOrders(success: { [weak self] array in
            guard let `self` = self else { return }
            self.orders = array
            self.tableView.reloadData()
            self.tableView.refreshControl?.endRefreshing()
            
        }, failure: { [weak self] in
            self?.tableView.reloadData()
            self?.tableView.refreshControl?.endRefreshing()
            self?.showAlert(title: "Ошибка", message: "Ошибка загрузки заказов", keyText: "Ок")
            
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "APOrderCell") as! APOrderCell
        let order = orders[indexPath.row]
        
        cell.roundedCell.layer.cornerRadius = 5
        cell.roundedCell.clipsToBounds = true
        
           
        cell.catImage.image = order.category.getCatPic()
        cell.titleLabel.text = order.product
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"

        cell.dateLabel.text = (order.acceptanceDate ?? Date()).stringVal
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: APCourierRouter.Courier.Segues.orderDetailSegue, sender: orders[indexPath.row])
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == APCourierRouter.Courier.Segues.orderDetailSegue{
            let destinationViewController = segue.destination as! APOrderScreenViewController
            destinationViewController.order = sender as! APOrderDto
        }
    }
  
}


