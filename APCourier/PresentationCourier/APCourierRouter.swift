//
//  APRouter.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 25.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation

class APCourierRouter {
    
    
    struct Client{
        struct ViewControllers {
            //ClientOrdersViewController
            public static var loginViewController = "APLoginViewController"
        }
        
        struct Segues {
            public static var successClientLoginSegue = "successClientLoginSegue"
            public static var orderDetailSegue = "orderDetailSegue"
            public static var naviAdressSegue = "naviAdressSegue"
            
        }
    }
    
    struct Courier{
        struct ViewControllers {
            public static var loginViewController = "APLoginViewController"
        }
        
        struct Segues {
            public static var successLoginSegue = "successLoginSegue"
            public static var orderDetailSegue = "orderDetailSegue"
            public static var naviAdressSegue = "naviAdressSegue"
            
        }
    }
    
    
    
    
    
    
    
}


