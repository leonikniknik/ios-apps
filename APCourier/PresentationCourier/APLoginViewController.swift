//
//  APLoginViewController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//


import UIKit

class APLoginViewController: UIViewController {
    
    @IBOutlet weak var loginField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginField.text = "test"
        passwordField.text = "test"
    }
    
    @IBAction func logIn(_ sender: Any) {
        guard
            let login = loginField.text,
            let password = passwordField.text
            else { return }
        APNetworkManager.login(login: login,
                               password: password,
                               onSuccess: { [weak self] dto in
                                guard let `self` = self else { return }
                                IS_LOGGED_IN = true
                                USER_ID = dto.id
                                USER = dto
                                self.performSegue(withIdentifier: APCourierRouter.Courier.Segues.successLoginSegue, sender: nil)
            },
                               onFailure: {
                                let alert = UIAlertController(title: "Внимание", message: "Неверная пара логин-пароль", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: { action in alert.dismiss(animated: true, completion: nil) }))
                                self.present(alert, animated: true, completion: nil)
        })
    }
}


