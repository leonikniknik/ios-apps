//
//  APNaviAdressViewController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//


import UIKit

class APNaviAdressViewController: UIViewController {
    
    var strUrl: String!
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Navi Adress"
        if let url = URL(string: strUrl) {
            webView.loadRequest(URLRequest(url: url))
        }
    }
}

