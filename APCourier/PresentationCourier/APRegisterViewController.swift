//
//  APRegisterViewController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class APRegisterViewController: UIViewController {
    
    @IBOutlet weak var loginField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var rePasswordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setupTransparentNavBar()
        loginField.text = "test"
        passwordField.text = "test"
        rePasswordField.text = "test"
        
        loginField.addTarget(self, action: #selector(validateForm(_:)), for: .editingChanged)
        
    }
    
    
    
    @objc func validateForm(_ sender: UITextField) {
//        if loginField.text ?? "" == "" {
//            
//        }
    }
    
    @IBAction func register(_ sender: Any) {
        guard
            let login = loginField.text,
            let password = passwordField.text,
            let rePassword = rePasswordField.text
            else { return }
        
        if password != rePassword {
            showAlert(title: "Ошибка", message: "Пароли не совпадают", keyText: "Ок")
            return
        }
        
        APNetworkManager.register(login: login,
                               password: password,
                               success: { [weak self] in
                                guard let `self` = self else { return }
                                IS_LOGGED_IN = false
                                USER_ID = -1
                                self.showAlert(title: "ОК", message: "ОК", keyText: "Ок") {
                                    self.navigationController?.popViewController(animated: true)
                                }
                                
            },
                               failure: { [weak self] in
                                guard let `self` = self else { return }
                                self.showAlert(title: "Ошибка", message: "Уже существует", keyText: "Ок")
//                                let alert = UIAlertController(title: "Внимание", message: "Неверная пара логин-пароль", preferredStyle: UIAlertControllerStyle.alert)
//                                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: { action in alert.dismiss(animated: true, completion: nil) }))
//                                self.present(alert, animated: true, completion: nil)
        })
    }
}

