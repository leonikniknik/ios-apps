//
//  APProfileViewController.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class APProfileViewController: UIViewController {
    

    @IBOutlet weak var geoSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Настройки профиля"
        geoSwitch.isOn = false
    }
    
    @IBAction func logOut(_ sender: Any) {
        USER_ID = -1
        IS_LOGGED_IN = false
        APProfileViewController.logOutFunc()
    }
    
    static func logOutFunc(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: APCourierRouter.Courier.ViewControllers.loginViewController)
        let nc = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.rootViewController = nc
    }
    
    @IBAction func sendDataSwitched(_ sender: UISwitch) {
        if sender.isOn {
            APCoordinateSender.startSending()
        } else {
            APCoordinateSender.startSending()
        }
    }
    
    
    
    
}

