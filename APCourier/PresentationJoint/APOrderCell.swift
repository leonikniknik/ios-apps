//
//  APOrderCell.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class APOrderCell: UITableViewCell {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var roundedCell: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var catImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        colorView.backgroundColor = UIColor.APColor.appViolet
        roundedCell.layer.borderColor = UIColor.black.cgColor
        roundedCell.layer.borderWidth = 1
        roundedCell.addShadow(shadowColor: .black)
    }

}
