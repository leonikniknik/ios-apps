//
//  UIViewControllerExtention.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 29/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class ASWViewController:UIViewController{
    
    
    var completion:(()->Void)?
    var visualEffectView: UIVisualEffectView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension UIViewController {
    //убрать таб бар
    
    //убрать прозрачность
    func setupBlackOpaqueNavBar() {
        navigationController?.navigationBar.barTintColor = UIColor.APColor.black
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barStyle = .blackOpaque
    }
    
    func addBackButton(animated: Bool = false) {
        let backButton: UIBarButtonItem
        if !animated {
            backButton = UIBarButtonItem(image: UIImage.backward, style: .done, target: self, action: #selector(goBackDefault))
        } else {
            backButton = UIBarButtonItem(image: UIImage.backward, style: .done, target: self, action: #selector(goBackAnimated))
        }
        self.navigationItem.setLeftBarButton(backButton, animated: false)
        
    }
    
    @objc func goBackDefault() {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func goBackAnimated() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func hideTabBarView() {
        self.tabBarController?.tabBar.isHidden = true
    }
}

final class ModalLoadingIndicator {
    
    static var currentOverlay: UIView?
    static var currentOverlayTarget: UIView?
    static var currentLoadingText: String?
    
    static func show() {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            return
        }
        show(currentMainWindow)
    }
    
    static func show(_ loadingText: String) {
        guard let currentMainWindow = UIApplication.shared.keyWindow else {
            return
        }
        show(currentMainWindow, loadingText: loadingText)
    }
    
    static func show(_ overlayTarget: UIView) {
        show(overlayTarget, loadingText: nil)
    }
    
    static func show(_ overlayTarget: UIView, loadingText: String?) {
        hide()
        
        let overlay = UIView(frame: overlayTarget.frame)
        overlay.center = overlayTarget.center
        overlay.alpha = 0
        overlay.backgroundColor = .black
        overlayTarget.addSubview(overlay)
        overlayTarget.bringSubview(toFront: overlay)
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        indicator.center = overlay.center
        indicator.startAnimating()
        overlay.addSubview(indicator)
        
        if let textString = loadingText {
            let label = UILabel()
            label.text = textString
            label.textColor = UIColor.white
            label.sizeToFit()
            label.center = CGPoint(x: indicator.center.x, y: indicator.center.y + 30)
            overlay.addSubview(label)
        }
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        overlay.alpha = overlay.alpha > 0 ? 0 : 0.5
        UIView.commitAnimations()
        
        currentOverlay = overlay
        currentOverlayTarget = overlayTarget
        currentLoadingText = loadingText
    }
    
    static func hide() {
        DispatchQueue.main.async {
            if currentOverlay != nil {
                currentOverlay?.removeFromSuperview()
                currentOverlay =  nil
                currentLoadingText = nil
                currentOverlayTarget = nil
            }
        }
        
    }
}



extension UIViewController {
    func setupTransparentNavBar() {
        UIApplication.shared.statusBarStyle = .lightContent
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

