//
//  APButtonManager.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 29/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class ASWButtonManager{
    static func setupButton(button:UIButton){
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        
        let image = UIImage.from(45, 45, 65)
        button.setBackgroundImage(image, for: .normal)
    }
    
    static func setupVKButton(button:UIButton){
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        
        var image = UIImage.from(37, 98, 147)
        button.setBackgroundImage(image, for: .normal)
        
        image = UIImage.from(19, 45, 64)
        button.setBackgroundImage(image, for: .selected)
        
        image = UIImage.from(234, 234, 234)
        button.setBackgroundImage(image, for: .disabled)
        
    }
    
    static func setupVioletButton(button:UIButton){
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        
        var image = UIImage.from(109, 151, 242)
        button.setBackgroundImage(image, for: .normal)
        
        image = UIImage.from(19, 45, 64)
        button.setBackgroundImage(image, for: .selected)
        
        image = UIImage.from(234, 234, 234)
        button.setBackgroundImage(image, for: .disabled)
        
    }
    
    static func setupLoginButton(button:UIButton){
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        
        let image = UIImage.from(45, 45, 65)
        button.setBackgroundImage(image, for: .normal)
        
        button.setTitleColor(.white, for: .normal)
//        image = UIImage.from(234, 234, 234)
//        button.setBackgroundImage(image, for: .disabled)
        
    }
}
