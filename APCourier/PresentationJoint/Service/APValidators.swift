//
//  APValidators.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 29/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import Foundation
class ASWEmailValidator {
    
    func isValid(_ string:String) -> Bool {
        return true
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
    
    func format(_ string: String) -> String {
        var result = string
        if string.characters.count > 255 {
            result = string.substring(to: string.index(string.startIndex, offsetBy: 255))
        }
        return result
    }
    
}


class ASWNameValidator {
    
    func isValid(_ string: String) -> Bool {
        return true
        return string.count > 1
    }
    
    
    func format(_ string: String) -> String {
        var result = string
        if string.characters.count > 255 {
            result = string.substring(to: string.index(string.startIndex, offsetBy: 255))
        }
        return result
    }
    
}


class ASWPasswordValidator  {
    
    func isValid(_ string:String) -> Bool {
        return true
        
        let emailRegEx = "[A-Z0-9a-z]{6,16}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
    
    func format(_ string: String) -> String {
        var result = string
        if string.characters.count > 16 {
            result = string.substring(to: string.index(string.startIndex, offsetBy: 16))
        }
        return result
    }
    
}
