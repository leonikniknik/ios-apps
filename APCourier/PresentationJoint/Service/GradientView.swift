//
//  GradientView.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 29/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

extension UIView {
    func addRoundCorner(cornerRadius: CGFloat) {
        layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
    func addShadow(shadowColor:UIColor){
        var shadowX: CGFloat = 1
        var shadowY: CGFloat = 3
        var shadowBlur: CGFloat = 3
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: 3, height: 3)
        layer.shadowRadius = 10
        layer.shadowOpacity = 1
    }
    func addGrad(topColor:UIColor, bottomColor:UIColor) {
    
    
    var gradientLayer: CAGradientLayer!

    var startPointX: CGFloat = 0.5
    
   var startPointY: CGFloat = 0
    
    var endPointX: CGFloat = 0.5
    var endPointY: CGFloat = 0.8

    gradientLayer = CAGradientLayer()
    gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
    gradientLayer.startPoint = CGPoint(x: startPointX, y: startPointY)
    gradientLayer.endPoint = CGPoint(x: endPointX, y: endPointY)
    gradientLayer.frame = self.bounds
    layer.addSublayer(gradientLayer)
    }
}
