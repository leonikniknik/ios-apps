//
//  Alerter.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 26.09.2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, keyText: String, action: (()->())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: keyText, style: UIAlertActionStyle.default, handler: { _ in
            action?()
            alert.dismiss(animated: true, completion: nil) }))
        self.present(alert, animated: true, completion: nil)
    }
}
