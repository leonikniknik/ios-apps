//
//  PlaceCell.swift
//  APCourier
//
//  Created by Aleksander Evtuhov on 30/09/2018.
//  Copyright © 2018 xxx. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var big: UIImageView!
    @IBOutlet weak var eat: UIImageView!
    
    @IBOutlet weak var small: UIImageView!
}
